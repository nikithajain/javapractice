package com.niki.interviews;
/**
 * mplement an LRU cache (use only core Java classes)	
 * Print t lines, each one in a new line corresponding to the result of the operation.
 * @author Nikitha 
 *
 */

import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class Wip01172LRU {

			// store keys of cache
			private Deque<Integer> doublyQueue;

			// store references of key in cache
			private HashSet<Integer> hashSet;

			// maximum capacity of cache 
			private final int CACHE_SIZE;

			Wip01172LRU(int capacity) {
				doublyQueue = new LinkedList();
				hashSet = new HashSet<>();
				CACHE_SIZE = capacity;
			}

			//refer the existing
			public void refer(int page) {
				if (!hashSet.contains(page)) {
					if (doublyQueue.size() == CACHE_SIZE) {
						int last = doublyQueue.removeLast();
						hashSet.remove(last);
					} 
				} 
				else {
					//remove the least accessed page
					doublyQueue.remove(page);
				} 
				doublyQueue.push(page);
				hashSet.add(page);
			}

			// display contents of cache
			public void display() {
				Iterator<Integer> itr = doublyQueue.iterator();
				while (itr.hasNext()) { 
					System.out.print(itr.next() + " "); 
				} 
			}

			public static void main(String[] args) {
				Wip01172LRU cache = new Wip01172LRU(4);
				cache.refer(1);
				cache.refer(2);
				cache.refer(3);
				cache.refer(1);
				cache.refer(4);
				cache.refer(5);
				cache.refer(2);
				cache.refer(2);
				cache.refer(1);
				cache.display();
			}

}
