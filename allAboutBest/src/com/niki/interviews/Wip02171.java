package com.niki.interviews;

import javax.management.RuntimeErrorException;

/**
 * Implement a big integer library, which supports basic 2 operations:
1) Addition
2) Subtraction
NOTE: DO NOT USE EXISTING BIG INTEGER LIBRARIES PROVIDED BY THE LANGUAGE USED.
Input:
You'll be given t(in the first line of input) test cases, each test case will have an operation to perform, in the format
OPERATOR OPERAND_1 OPERAND_2
for e.g. 
ADD 7 5, and the result would be 12
SUB -3 -7 and the result would be 4
Next t lines contains 3 inputs each, corresponding to the operator and 2 operands.
 * @author Nikitha
 *
 */
/**
 * Approach -- custom design BigInt class use strings for computation
 * 
 *
 */

public class Wip02171 {

	public static void main(String[] args) {
		String operation = args[0];
		BigInt firstOp = new BigInt(args[1]);
		BigInt secondOp= new BigInt(args[2]);
		String solution;
		
		if(operation.equalsIgnoreCase("ADD")) {
			solution = BigInt.add(firstOp, secondOp).getValue();
			System.out.println("Solution is "+solution);
		}
		else if(operation.equalsIgnoreCase("SUB")) {
			solution = BigInt.sub(firstOp, secondOp).getValue();
			System.out.println("Solution is "+solution);
		}
		else {
			throw new RuntimeException("Operator not reconized");
		}
	}

}
class BigInt implements Comparable<BigInt>{
	private String value;
	
	//Constructor
	BigInt(String value){
		this.value=value;
	}
	
	//gettters and setters
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	//common util,
	public char[] chars(){
		return this.value.toCharArray();
	}
	
	public int lenght() {
		return this.value.length();
	}

	//need to compare values
	@Override
	public int compareTo(BigInt o) {
		if(this.lenght()>o.lenght())	return 1;
		if(this.lenght()<o.lenght())	return -1;
		char[] thisChars = this.chars();
		char[] oChars = o.chars();
		for(int i=0; i<this.lenght();i++) {
			int thisInt = Character.getNumericValue(thisChars[i]);
			int oInt = Character.getNumericValue(oChars[i]);
			
			if(thisInt>oInt)	return 1;
			if(thisInt<oInt)	return -1;
		}
		return 0;
	}
	
	//to make both operators equal lenght to make it easier for addition or sub
	private static String[] equallengths(BigInt x,BigInt y) {
		String xVal=x.getValue();
		String yVal=y.getValue();
		
		if(x.lenght()>y.lenght()) {
			xVal = x.getValue();
			String zeroes = addzeroes(x.lenght()-y.lenght());
			yVal = zeroes+y.getValue();
		}else if(x.lenght()<y.lenght()) {
			yVal = y.getValue();
			String zeroes = addzeroes(y.lenght()-x.lenght());
			xVal = zeroes+x.getValue();
		}
		return new String[] {xVal,yVal};
	}
	
	private static String addzeroes(int number) {
		StringBuilder s = new StringBuilder();
		for(int i=0;i<number ;i++) {
			s.append("0");
		}
		return s.toString();
	}
	
	//Addition
	public static BigInt add(BigInt x, BigInt y) {
		String[] xy = equallengths(x, y);
		char[] xChars = xy[0].toCharArray();
		char[] yChars = xy[1].toCharArray();
		
		int carry =0;
		
		StringBuilder sol = new StringBuilder();
		
		for(int i=xChars.length-1;i>=0;i--) {
			int xInt = Character.getNumericValue(xChars[i]);
			int yInt = Character.getNumericValue(yChars[i]);
			int digitSum = xInt+yInt+carry;
			sol.append(digitSum%10);
			carry = digitSum/10;
		}
		
		return new BigInt(carry==0?"":"1"+sol.reverse().toString());
		
	}
	
	//Subtraction
		public static BigInt sub(BigInt x, BigInt y) {
			int xGreaty = x.compareTo(y);
			if(xGreaty == 0) return new BigInt("0");// both are equal so difference will be 0
			else if(xGreaty <0)	throw new RuntimeException("negative Scenario not implemented");
			else {
				String[] xy = equallengths(x, y);
				char[] xChars = xy[0].toCharArray();
				char[] yChars = xy[1].toCharArray();
				
				int borrow =0;
				StringBuilder sol = new StringBuilder();
				for(int i=xChars.length-1;i>=0;i--) {
					int xInt = Character.getNumericValue(xChars[i]);
					int yInt = Character.getNumericValue(yChars[i])+borrow;
					borrow =0;
					if(xInt<yInt) {
						borrow =-1;
						yInt+=10;
					}
					sol.append(yInt-xInt);
				}
				//delete appended zeroes if any
				while(sol.charAt(sol.length()-1)==0)
					sol.deleteCharAt(sol.length()-1);
				
				
				return new BigInt(sol.reverse().toString());
				
			}
			
		}
}
